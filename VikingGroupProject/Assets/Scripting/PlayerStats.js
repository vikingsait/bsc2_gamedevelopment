﻿#pragma strict
import UnityEngine.UI;

var healthbar:Image;
var playerHealth:float;
var playerHealthMax:float;

function Start () 
{
	playerHealth = 3;
	playerHealthMax=3;
}

function Update () 
{
	if(Input.GetKey(KeyCode.Escape))
	{
		Application.LoadLevel("Menu");
	}
	healthbar.fillAmount = playerHealth/playerHealthMax;
	if(playerHealth<=0)
	{
		Application.LoadLevel("GameOver");
	}
}
function HitPlayer(dmg:int)
{
	playerHealth = playerHealth-dmg;
}