﻿#pragma strict
private var thisParticleSystem:ParticleSystem;
function Start () {
 thisParticleSystem = GetComponent(ParticleSystem);
}

function Update () {
	if(thisParticleSystem.isPlaying)
		return;
		
	Destroy(gameObject);
}