﻿#pragma strict
var enemyHealth:int = 0;
var colParticles:GameObject;
var moveSpeed:float;
var setSpeed:float;

private var myRigidbody2D:Rigidbody2D;
private var anim:Animator;

var isWalking:boolean;

var range : float=5;
var stop : float=2;
 
var stopForAWhile:boolean;

private var walkCounter:float;
var walkTime:float;

private var waitCounter:float;
var waitTime:float;

private var WalkDirection:int;

var target : Transform;
var myTransform : Transform;

 function Awake()
 {
     myTransform = transform; 
 }
function Start()
{
	myRigidbody2D=GetComponent(Rigidbody2D);
	anim = GetComponent(Animator);
	
	waitCounter=waitTime;
	walkCounter=walkTime;
	
	target = GameObject.FindWithTag("Player").transform; //target the player
	
	ChooseDirection();
}
function Update()
{
	if(enemyHealth<=0)
	{
		Destroy(gameObject);
		Instantiate(colParticles, transform.position, transform.rotation);
	}
	
	var distance = Vector3.Distance(myTransform.position, target.position);
   
	if(distance<=range && distance>stop&&stopForAWhile)
	{
		isWalking=false;
		moveSpeed=setSpeed;
	     transform.position = Vector2.MoveTowards(transform.position, target.position, moveSpeed * Time.deltaTime);
    }
    else if(distance<=stop)
    {
    	myRigidbody2D.velocity=Vector2.zero;
    	waitCounter=15;
    	stopForAWhile=false;
    	isWalking=false;
		moveSpeed=0;
    }
	else if(isWalking&&distance>range)
	{
		switch(WalkDirection)
		{
			case 0:
				myRigidbody2D.velocity = new Vector2(0, moveSpeed);
				anim.SetBool("right", false);
				anim.SetBool("left", false);
				anim.SetBool("up", true);
				anim.SetBool("down", false);
				break;
			case 1:
				myRigidbody2D.velocity = new Vector2(moveSpeed, 0);
				anim.SetBool("right", true);
				anim.SetBool("left", false);
				anim.SetBool("up", false);
				anim.SetBool("down", false);
				break;	
			case 2:
				myRigidbody2D.velocity = new Vector2(0, -moveSpeed);
				anim.SetBool("right", false);
				anim.SetBool("left", false);
				anim.SetBool("up", false);
				anim.SetBool("down", true);
				break;
			case 3:
				myRigidbody2D.velocity = new Vector2(-moveSpeed, 0);
				anim.SetBool("right", false);
				anim.SetBool("left", true);
				anim.SetBool("up", false);
				anim.SetBool("down", false);
				break;
		}
		walkCounter-=Time.deltaTime;
		if(walkCounter<0)
		{
			isWalking=false;
			waitCounter = waitTime;
		}
	}
	else
	{
		waitCounter -=Time.deltaTime;
		myRigidbody2D.velocity=Vector2.zero;
		stopForAWhile=true;
		if(waitCounter<0)
		{
			ChooseDirection();
		}
	}
}
function ChooseDirection()
{
	WalkDirection = Random.Range(0, 4);
	isWalking=true;
	walkCounter = walkTime;
}

function HitEnemy(dmg:int)
{
	Instantiate(colParticles, transform.position, transform.rotation);
	enemyHealth=enemyHealth-dmg;
}