﻿#pragma strict

public var audioClip: AudioClip[];
private var speed:float = 3;
private var axeSpeed:float = 5;
private var axeSpeedd:float = -360;
var right:boolean = false;
var idle:boolean = false;
var up:boolean = false;
var down:boolean = false;
var animator:Animator;
var axe:Rigidbody2D;
var shotDelay:float;
var shotDelayCounter:float;
var rotationSpeed:float;

function Start () 
{	
	animator = GetComponent(Animator);	
}

function OnGUI()
{
	GUI.Box (Rect(10,10,120,25), "Backpack: " + TreasureCollect.coins + "/10");
}

function OnCollisionEnter2D(other:Collision2D)
{
	if(other.transform.gameObject.tag=="dropCoins" && TreasureCollect.coins == 10)
	{
		TreasureCollect.coins = 0;
		Application.LoadLevel(Application.loadedLevel+1);
	}
	if(other.transform.gameObject.tag=="Treasure")
	{
		PlaySound(0);
	}
}

function PlaySound(clip:int)
{
	GetComponent.<AudioSource>().clip = audioClip[clip];
	GetComponent.<AudioSource>().Play();
}

function Update () 
{
	Physics2D.IgnoreLayerCollision(8, 9,true);
	playerMovement();
}


function playerMovement()
{
	var axeInstance:Rigidbody2D;
	animator.speed=0;
	if (Input.GetKey(KeyCode.W))
	{
		transform.Translate(Vector3.up*speed*Time.deltaTime);
		animator.SetBool("right", false );
		animator.SetBool("down", false );
		animator.SetBool("up", true );
		down = false;
		right = false;
		up = true;
		animator.speed=1;
	}
	

	if (Input.GetKey(KeyCode.A))
	{
		transform.Translate(Vector3.left*speed*Time.deltaTime);
		transform.localScale= new Vector3(-1,1,1);
		animator.SetBool("right", true );
		animator.SetBool("down", false );
		animator.SetBool("up", false );
		down = false;
		right = true;
		up = false;
		animator.speed=1;
	}
	
	
	if (Input.GetKey(KeyCode.D))
	{
		animator.SetBool("right", true );
		animator.SetBool("down", false );
		animator.SetBool("up", false );
		down = false;
		right = true;
		up = false;
		transform.Translate(Vector3.right*speed*Time.deltaTime);
		transform.localScale=new Vector3(1,1,1);
		animator.speed=1;
	}

	if (Input.GetKey(KeyCode.S))
	{
		animator.SetBool("right", false );
		animator.SetBool("down", true );
		animator.SetBool("up", false );
		down = true;
		right = false;
		up = false;
		transform.Translate(Vector3.down*speed*Time.deltaTime);
		animator.speed=1;
	}
	if(Input.GetKeyDown(KeyCode.K))
	{
		shotDelayCounter=shotDelay;
		axeInstance = Instantiate(axe, transform.position, Quaternion.Euler(new Vector3(1,0,0)));
		if(right==true)
		{
			if(transform.localScale.x<0)
			{
				axeInstance.velocity = new Vector2(-axeSpeed,0);
				axeInstance.angularVelocity = -axeSpeedd;
			}
			else{
				axeInstance.velocity = new Vector2(axeSpeed,0);
				axeInstance.angularVelocity = axeSpeedd;
			}
		}
		if(up==true)
		{
			axeInstance.velocity = new Vector2(0,axeSpeed);
			axeInstance.angularVelocity = axeSpeedd;
		}
		if(down==true)
		{
			axeInstance.velocity = new Vector2(0,-axeSpeed);
			axeInstance.angularVelocity = axeSpeedd;
		}
	}
	if(Input.GetKey(KeyCode.K))
	{
		shotDelayCounter-=Time.deltaTime;
		if(shotDelayCounter<=0)
		{
			shotDelayCounter=shotDelay;
			axeInstance = Instantiate(axe, transform.position, Quaternion.Euler(new Vector3(1,0,0)));
			if(right==true)
			{
				if(transform.localScale.x<0)
				{
					axeInstance.velocity = new Vector2(-axeSpeed,0);
				}
				else{
					axeInstance.velocity = new Vector2(axeSpeed,0);
				}
			}
			if(up==true)
			{
				axeInstance.velocity = new Vector2(0,axeSpeed);
			}
			if(down==true)
			{
				axeInstance.velocity = new Vector2(0,-axeSpeed);
			}
		}
	}
	
}


