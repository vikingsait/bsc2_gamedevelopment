﻿#pragma strict
var dmg:int=1;
function OnCollisionEnter2D(other:Collision2D)
{
	if(other.transform.gameObject.tag=="Wall")
	{
		Destroy(gameObject);
	}
	if(other.transform.gameObject.tag=="Treasure")
	{
		Destroy(gameObject);
	}
	if(other.transform.gameObject.tag=="Enemy")
	{
		Destroy(gameObject);
		other.gameObject.GetComponent(Enemy).HitEnemy(dmg);
	}
}
