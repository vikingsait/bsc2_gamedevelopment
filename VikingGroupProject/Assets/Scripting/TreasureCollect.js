﻿#pragma strict

static var coins: int = 0;
var anim: Animator;

var up: boolean;
var down: boolean;


var tresure : Transform;
private var hero : Transform;
var colParticles:GameObject;
function Start() 
{
	anim = GetComponent("Animator"); 
	up = false;
	down = false;
	coins=0;
}

function Awake()
{
	anim = GetComponent("Animator");
	hero = GameObject.FindWithTag("Player").transform;
}

function Update () 
{
	var dist = Vector3.Distance(tresure.position, hero.position);
	
	if(dist < 3 && up==false && coins +1 <=10)
	{
		anim.SetBool("Collected", true);
		anim.SetBool("NotCollected", false);
		up=true;
		down=false;
	}
	if(dist > 2 && up==true)
	{
		anim.SetBool("Collected", false);
		anim.SetBool("NotCollected", true);
		up=false;
		down=true;
	}
}

function OnCollisionEnter2D(other:Collision2D)
{
	if (other.gameObject.name == "Hero")
	{
		if (coins + 1 <= 10)
		{
			Instantiate(colParticles, transform.position, transform.rotation);
			coins += 1;
			Instantiate(colParticles, transform.position, transform.rotation);
			Destroy(gameObject);
		}
	}
}